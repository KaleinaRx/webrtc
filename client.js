(function () {

    var lastPeerId = null;
    var peer = null; // own peer object
    var conn = null;
    var call = null;
    var callserver = [];
    var incomingCall = null;
    var rooms = null;
    var contacts = [];
    var bandwidth = 20000;
    const serverKey = "fiKz3Hy7aQmc1091I9cS1ImSa5QlxZp812T01p";
    
    var username = document.getElementById('username');
    var recvId = document.getElementById('own-id');
    var videoout = document.getElementById('videoframe');
    var videoin = document.getElementById('videoframe2');
    var recvIdInput = document.getElementById("receiver-id");
    var status = document.getElementById("status");
    var message = document.getElementById("message");
    var sendMessageBox = document.getElementById("sendMessageBox");
    var sendButton = document.getElementById("sendButton");
    var clearMsgsButton = document.getElementById("clearMsgsButton");
    var connectButton = document.getElementById("connect-button");
    var videoCallButton = document.getElementById("videoCallButton");
    var callButton = document.getElementById("callButton");
    var answerAudioButton = document.getElementById("answerAudioButton");
    var answerVideoButton = document.getElementById("answerVideoButton");
    var closeCallButton = document.getElementById("closeCallButton");
    var connect2serverButton = document.getElementById('connect2server-button');
    var disconnectButton = document.getElementById('disconnect-button');
    var roomTable = document.getElementById('rooms');
    var callPeersOnServerButton = document.getElementById('callPeersOnServer');
    var closeCallOnServerButton = document.getElementById('closeCallOnServer');
    var roomButton = [];
    var selfvideo = null;

    /**
     * Create the Peer object.
     *
     * Sets up callbacks that handle any events related to our
     * peer object.
     */
    function initialize() {

        callButton.disabled = true;
        videoCallButton.disabled = true;
        disconnectButton.disabled = true;
        answerAudioButton.style.display = "none";
        answerVideoButton.style.display = "none";
        closeCallButton.style.display = "none";
        callPeersOnServerButton.style.display = "none";
        closeCallOnServerButton.style.display = "none";

        // Create own peer
        peer = new Peer(null, {
            debug: 2
        });

        //Connection enstablished event
        peer.on('open', function (id) {
            // Workaround for peer.reconnect deleting previous id
            if (peer.id === null) {
                console.log('Received null id from peer open');
                peer.id = lastPeerId;
            } else {
                lastPeerId = peer.id;
            }

            console.log('ID: ' + peer.id);
            recvId.innerHTML = "ID: " + peer.id;
            status.innerHTML = "Awaiting connection...";
        });
       
        //Incoming connection event
        peer.on('connection', function (c) {
            //Check if it's already connected
            if (conn && conn.open) {
                c.on('open', function() {
                    c.send("Already connected to another client");
                    setTimeout(function() { c.close(); }, 500);
                });
                return;
            }
            conn = c;
            status.innerHTML = "Connected";
            videoCallButton.disabled = false;
            callButton.disabled = false;
            dataHandler();
        });

        //Disconnect event
        peer.on('disconnected', function () {
            status.innerHTML = "Connection lost. Please reconnect";
            console.log('Connection lost. Please reconnect');

            // Workaround for peer.reconnect deleting previous id
            peer.id = lastPeerId;
            peer._lastServerId = lastPeerId;
            peer.reconnect();
        });

        //Closing connection event
        peer.on('close', function() {
            conn = null;
            status.innerHTML = "Connection destroyed. Please refresh";
            console.log('Connection destroyed');
        });

        //Errors event
        peer.on('error', function (err) {
            console.log(err);
            //alert('' + err);
            if (err.type == 'peer-unavailable') {
                if (err.toString().search(serverKey)) {
                    status.innerHTML = status.innerHTML + "<br/>Server unavailable";
                } else {
                    status.innerHTML = status.innerHTML + "<br/>Peer unavailable";
                }
            }
        });
    };

    /**
     * Create the connection between the two Peers.
     *
     * Sets up callbacks that handle any events related to the
     * connection and data received on it.
     */
    function join(id) {
        // Close old connection
        if (conn) {
            conn.close();
        }

        // Create connection to destination peer specified in the input field
        conn = peer.connect(id , {
            reliable: true
        });

        //Not working as expected
        if (conn != null) {
            callButton.disabled = false;
            videoCallButton.disabled = false;
            disconnectButton.disabled = false;
        }

        dataHandler();

        /*//Handles incoming data (messages only)
        conn.on('data', function (data) {
            addMessage("<span class=\"peerMsg\">Peer:</span> " + data);
        });*/                 
    };

    /**
     * Get first "GET" parameter from href.
     * This enables delivering an initial command upon page load.
     */
    function getUrlParam(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return null;
        else
            return results[1];
    };

    /**
    * Starts a call towards the other peer connected
    */
    function callpr() {
        var constraints = { audio: true, video: false }; 

        //Gets user microphone
        navigator.mediaDevices.getUserMedia(constraints)
        .then(function(mediaStream) {
            call = peer.call(conn.peer, mediaStream, {sdpTransform: (sdp) => { 
                console.log(sdp);
                return updateBandwidthRestriction(sdp, bandwidth);
            }});
            callButton.disabled = true;
            videoCallButton.disabled = true;
            closeCallButton.style.display = "inline";
            receivePeerVideo();
        })
        .catch(function(err) { console.log(err.name + ": " + err.message); });
    }
    
    /**
    * Starts a videocall towards the other peer connected
    */
    function videoCallpr() {
        var constraints = { audio: true, video: { width: 854, height: 480 } }; 

        //Gets the user camera
        navigator.mediaDevices.getUserMedia(constraints)
        .then(function(mediaStream) {
            videoout.srcObject = mediaStream;
            videoout.onloadedmetadata = function(e) {
            videoout.play();
            call = peer.call(conn.peer, mediaStream, {sdpTransform: (sdp) => { 
                console.log(updateBandwidthRestriction(sdp, bandwidth));
                return updateBandwidthRestriction(sdp, bandwidth);
            }});
            videoCallButton.disabled = true;
            callButton.disabled = true;
            closeCallButton.style.display = "inline";
            receivePeerVideo();
            };
        })
        .catch(function(err) { console.log(err.name + ": " + err.message); });
        
    }

    /**
    * Receves the other peer media stream
    */
    function receivePeerVideo() {
        call.on('stream', function(stream) {
            // `stream` is the MediaStream of the remote peer.
            videoin.srcObject = stream;
            videoin.play();
            console.log("the incoming video should be playing");
            console.log("video src: " + stream.getTracks());
        });
    }

    /**
    * Receves the other peer media stream for the answerer
    */
    function receivePeerVideoForAnswer() {
        incomingCall.on('stream', function(stream) {
            // `stream` is the MediaStream of the remote peer.
            videoin.srcObject = stream;
            videoin.onloadedmetadata = function(e) {
            videoin.play();
            }
        });
    }

    /**
    * Handles data and calls
    */
    function dataHandler() {

        //Writes onscreen to whom we are connected
        conn.on('open', function () {
            if (conn.peer != serverKey) {
                status.innerHTML = "Connected to: " + conn.peer;
            } else {
                status.innerHTML = "Connected to: Server";
                callPeersOnServerButton.style.display = "inline";
                closeCallOnServerButton.style.display = "inline";
            }
            console.log("Connected to: " + conn.peer);

            // Check URL params for comamnds that should be sent immediately
            var command = getUrlParam("command");
            if (command) {
                //conn.send(command);
            }
        });

        //Closes connection
        conn.on('close', function () {
            status.innerHTML = "Connection closed";
            conn = null;
        });

        //Handle incoming data (messages only)
        conn.on('data', function (data) {
            console.log(data);
            if (data == "/closecall") {
                //closeCall();
            } else if (Array.isArray(data)) {
                contacts = data;
                console.log("got contacts");
                callContacts();
                //drawRooms();
            } else {
                addMessage(data);
            }
        });

        //Answer the call, providing the mediaStream
        try {
            peer.on('call', function(calll) {
                console.log("call event ok");
                if (conn.peer != serverKey) {
                    status.innerHTML = status.innerHTML + "<br/>Receiving incoming call";
                    videoCallButton.disabled = true;
                    callButton.disabled = true;
                    answerAudioButton.style.display="inline";
                    answerVideoButton.style.display="inline";
                    incomingCall = calll;
                } else {
                    //server answer
                    var count = 0;
                    while (callserver[count] != null) {
                        count++;
                    }
                    callserver[count] = calll;
                    console.log("answering peer: " + calll.peer);
                    answerServerCall(count);
                    count = 0;
                }
            });
        } catch(err) {
            console.log(err.name + ": " + err.message);
        }

        //Checks if the call is closed
        if (call != null) {
            call.on('close', function() {
                closeCallButton.style.display = "none";
                videoCallButton.disabled = false;
                callButton.disabled = false;
                status.innerHTML = status.innerHTML + "<br/>Call closed";
            });
        }  
        
        //Checks if the call is closed
        if (incomingCall != null) {
            incomingCall.on('close', function() {
                closeCallButton.style.display = "none";
                videoCallButton.disabled = false;
                callButton.disabled = false;
                status.innerHTML = status.innerHTML + "<br/>Call closed";
            });
        } 
    }

    /**
     * Gets user microphone
     */
    function getMicrophone() {
        var constraints = { audio: true, video: false };

            //Gets user device
            navigator.mediaDevices.getUserMedia(constraints)
            .then(function(mediaStream) {
                selfvideo = document.createElement('video');
                selfvideo.srcObject = mediaStream;
            })
            .catch(function(err) { console.log(err.name + ": " + err.message); });
    }

    /**
     * Gets user webcam and microphone
     */
    function getWebcam() {
        var constraints = { audio: true, video: { width: 854, height: 480 } };

            //Gets user device
            navigator.mediaDevices.getUserMedia(constraints)
            .then(function(mediaStream) {
                selfvideo = document.createElement('video');
                selfvideo.srcObject = mediaStream;
            })
            .catch(function(err) { console.log(err.name + ": " + err.message); });
    }

    /**
     * Requests the server contact list
     */
    function getContacts() {
        
        conn.send("/sendcontacts");
        console.log("contacts requested");
        videoCallButton.disabled = true;
        callButton.disabled = true;
        callPeersOnServerButton.disabled = true;
    }

    /**
     * Calls the peers on the server contact list
     */
    function callContacts() {
        var count = 0;
        contacts.forEach(contact => {
            var checkcall = true;
            callserver.forEach(callelement => {
                if (callelement.peer == contact) {
                    checkcall = false;
                }   
            });
            if (checkcall) {
                while (callserver[count] != null) {
                    count++;
                }
                callserver[count] = peer.call(contact, selfvideo.srcObject, {sdpTransform: (sdp) => { 
                    return updateBandwidthRestriction(sdp, bandwidth);
                }});
                console.log("calling: " + contact);
                receivePeerVideoForServerAnswer(count);
            }
        });
    }

    /**
     * Answers the calls coming from other peers connected to the server
     * @param {*} count 
     */
    function answerServerCall(count) {
        console.log("trying to answer");
        callserver[count].answer(selfvideo.srcObject, {sdpTransform: (sdp) => { 
            return updateBandwidthRestriction(sdp, bandwidth);
        }});
        console.log("answer successful");
        receivePeerVideoForServerAnswer(count);
    }

    /**
     * Closes the call between peers connected to the server.
     * 
     * Warning: until you disconnect from the server the client can
     *          still accept connections from other clients.
     */
    function closeCallToServer() {
        callserver.forEach(element => {
            try {
                element.close();
                element = null;
            } catch (err) {
                console.log(err.name + ": " + err.message);
            }
        });
        callPeersOnServerButton.disabled = false;
    }

    /**
     * Audio playback when receiving an audio stream from the server
     */
    function receivePeerVideoForServerAnswer(count) {
        callserver[count].on('stream', function(stream) {
            // `stream` is the MediaStream of the remote peer.
            const video = document.createElement('video');
            video.srcObject = stream;
            video.onloadedmetadata = function(e) {
            video.play();
            }
        });
    }

    /**
    * Answers an incoming call with audio only
    */
    function answerAudioCall() {
        var constraints = { audio: true, video: false };

            //Gets user microphone
            navigator.mediaDevices.getUserMedia(constraints)
            .then(function(mediaStream) {
                incomingCall.answer(mediaStream, {sdpTransform: (sdp) => { 
                    return updateBandwidthRestriction(sdp, bandwidth);
                }});
                videoCallButton.disabled = true;
                callButton.disabled = true;
                answerAudioButton.style.display = "none";
                answerVideoButton.style.display = "none";
                closeCallButton.style.display = "inline";
                receivePeerVideoForAnswer();                            
            })
            .catch(function(err) { console.log(err.name + ": " + err.message); });
    }

    /**
    * Answers an incoming call with audio and video
    */
    function answerVideoCall() {
        var constraints = { audio: true, video: { width: 854, height: 480 } };

            //Gets user camera
            navigator.mediaDevices.getUserMedia(constraints)
            .then(function(mediaStream) {
                videoout.srcObject = mediaStream;
                videoout.onloadedmetadata = function(e) {
                videoout.play();
                incomingCall.answer(mediaStream, {sdpTransform: (sdp) => { 
                    return updateBandwidthRestriction(sdp, bandwidth);
                }});
                videoCallButton.disabled = true;
                callButton.disabled = true;
                answerAudioButton.style.display = "none";
                answerVideoButton.style.display = "none";
                closeCallButton.style.display = "inline";
                receivePeerVideoForAnswer();                            
                };
            })
            .catch(function(err) { console.log(err.name + ": " + err.message); });
    }

    /**
    * Closes the ongoing call
    */
    function closeCall() {
        if (call != null) {
            try {
                call.close();  
                call = null;                 
            } catch(err) {
                console.log(err.name + ": " + err.message);
            }
        }
        
        
        if (incomingCall != null) {
            try {
                incomingCall.close();
                incomingCall = null;
            } catch(err) {
                console.log(err.name + ": " + err.message);
            }
        }
        
                    
        if (call == null && incomingCall == null) {
            videoout.srcObject = null;
            videoin.srcObject = null;
            closeCallButton.style.display = "none";
            videoCallButton.disabled = false;
            callButton.disabled = false;
            status.innerHTML = status.innerHTML + "<br/>Call closed";
        }
    }

    /**
    * Prints messages into the chat
    */
    function addMessage(msg) {
        var now = new Date();
        var h = now.getHours();
        var m = addZero(now.getMinutes());
        var s = addZero(now.getSeconds());

        function addZero(t) {
            if (t < 10)
                t = "0" + t;
            return t;
        };
        var msg2 = msg.split("/split");
        message.innerHTML = message.innerHTML + "<br><span class=\"msg-time\">" + h + ":" + m + ":" + s + "</span>  -  " + msg2[0] + msg2[1];
        message.scrollTop = message.scrollHeight;
    };

    function clearMessages() {
        message.innerHTML = "";
    };

    function disconnect() {
        conn.close();
        conn = null;
        closeCall();
        status.innerHTML = "Connection closed";
    }

    /**
     * Draws the server rooms on the interface
     */
    function drawRooms() {
        var count = 0;
        rooms.forEach(element => {
            roomTable.innerHTML = roomTable.innerHTML + "<tr><td><button id=" + element + ">" + element + "</button></td></tr>";
            roomButton[count] = document.getElementById(element);
            count++;
        });  
    }

    // Listen for enter in message box
    sendMessageBox.addEventListener('keypress', function (e) {
        var event = e || window.event;
        var char = event.which || event.keyCode;
        if (char == '13')
            sendButton.click();
    });
    // Send message
    sendButton.addEventListener('click', function () {
        if (conn && conn.open) {
            var msg;
            if (username.value != "") {
                msg = username.value + ": /split" + sendMessageBox.value;
            } else {
                msg = "User: /split" + sendMessageBox.value;
            }
            sendMessageBox.value = "";
            conn.send(msg);
            console.log("Sent: " + msg);
            addMessage(msg);
        } else {
            console.log('Connection is closed');
        }
    });

    /**
     * Modifies the sdp with the specified bandwidth in kbs
     * 
     * @param {*} sdp 
     * @param {*} bandwidth 
     */
    function updateBandwidthRestriction(sdp, bandwidth) {
        let modifier = 'AS';
        if (adapter.browserDetails.browser === 'firefox') {
            bandwidth = (bandwidth >>> 0) * 1000;
            modifier = 'TIAS';
        }
        if (sdp.indexOf('b=' + modifier + ':') === -1) {
          // insert b= after c= line
          sdp = sdp.replace(/c=IN (.*)\r\n/g, 'c=IN $1\r\nb=' + modifier + ':' + bandwidth + '\r\n');
        } else {
          sdp = sdp.replace(new RegExp('b=' + modifier + ':.*\r\n'), 'b=' + modifier + ':' + bandwidth + '\r\n');
        }
        return sdp;
    }

    
    //Call button event
    videoCallButton.addEventListener('click', videoCallpr);
    callButton.addEventListener('click', callpr);

    //Call Peers on Server event
    callPeersOnServerButton.addEventListener('click', getContacts);

    //Answer call button event
    answerAudioButton.addEventListener('click', answerAudioCall);
    answerVideoButton.addEventListener('click', answerVideoCall);

    //Close call button event
    closeCallButton.addEventListener('click', closeCall);

    //Close call on server button event
    closeCallOnServerButton.addEventListener('click', closeCallToServer);

    //Clear messages box
    clearMsgsButton.addEventListener('click', clearMessages);
    //Start peer connection on click
    connectButton.addEventListener('click', function() { join(recvIdInput.value) });
    connect2serverButton.addEventListener('click', function() { join(serverKey); getMicrophone(); });
    
    //Disconnect from connected peer
    disconnectButton.addEventListener('click', disconnect);

    // Since all callbacks are setup, start the process of obtaining an ID
    initialize();
})();